package model;

/**
 * 
 * @author User-06
 *
 */
public class Achat {

	private Integer idAchat;
	private String nomClient;
	private String prenomClient;
	private String secu;
	private String medecin;
	private String dateAchat;
	private String specialiste;
	private Medicament medicament;
	private String mutuelle;
	private Integer quantiteMedicament;
	private Double prix;
	private Double remboursement;
	private Double prixTotal;

	/**
	 * 
	 * @param idAchat            id pour les achats
	 * @param nomClient          Client nom de Client
	 * @param prenomClient       prenom de client
	 * @param secu               numéro de sécurité sociale
	 * @param medecin            le médecin traitant
	 * @param dateAchat          date d'Achat
	 * @param specialiste        Spécialiste
	 * @param medicament         Médicament
	 * @param mutuelle           Mutuelle
	 * @param quantiteMedicament la quantité des médicaments
	 * @param prix               le prix des médicaments
	 * @param remboursement      le pourcentage de remboursement
	 * @param prixTotal          le prix total d'achat
	 */
	public Achat(Integer idAchat, String nomClient, String prenomClient, String secu,
			String medecin, String dateAchat, String specialiste, Medicament medicament,
			String mutuelle, Integer quantiteMedicament, Double prix, Double remboursement,
			Double prixTotal) {
		setNomClient(nomClient);
		setPrenomClient(prenomClient);
		setSecu(secu);
		setMedecin(medecin);
		setDateAchat(dateAchat);
		setSpecialiste(specialiste);
		setMedicament(medicament);
		setMutuelle(mutuelle);
		setQuantiteMedicament(quantiteMedicament);
		setPrixTotal(prixTotal);
		setRemboursement(remboursement);
		setPrix(prix);
	}

	/**
	 * 
	 * @param nomClient          Client nom de Client
	 * @param prenomClient       prenom de client
	 * @param secu               numéro de sécurité sociale
	 * @param medecin            le médecin traitant
	 * @param dateAchat          date d'Achat
	 * @param specialiste        Spécialiste
	 * @param medicament         Médicament
	 * @param mutuelle           Mutuelle
	 * @param quantiteMedicament la quantité des médicaments
	 * @param prix               le prix des médicaments
	 * @param remboursement      le pourcentage de remboursement
	 * @param prixTotal          le prix total d'achat
	 */
	public Achat(String nomClient, String prenomClient, String secu, String medecin,
			String dateAchat, String specialiste, Medicament medicament, String mutuelle,
			Integer quantiteMedicament, Double prix, Double remboursement, Double prixTotal) {
		setNomClient(nomClient);
		setPrenomClient(prenomClient);
		setSecu(secu);
		setMedecin(medecin);
		setDateAchat(dateAchat);
		setSpecialiste(specialiste);
		setMedicament(medicament);
		setMutuelle(mutuelle);
		setQuantiteMedicament(quantiteMedicament);
		setPrixTotal(prixTotal);
		setRemboursement(remboursement);
		setPrix(prix);
	}

	/**
	 * 
	 * @return le prix
	 */
	public Double getPrix() {
		return prix;
	}

	/**
	 * 
	 * @param prix
	 */
	public void setPrix(Double prix) {
		this.prix = prix;
	}

	/**
	 * 
	 * @return remboursement
	 */
	public Double getRemboursement() {
		return remboursement;
	}

	/**
	 * 
	 * @param remboursement
	 */
	public void setRemboursement(Double remboursement) {
		this.remboursement = remboursement;
	}

	/**
	 * 
	 * @return idAchat
	 */
	public Integer getIdAchat() {
		return idAchat;
	}

	/**
	 * 
	 * @param idAchat
	 */
	public void setIdAchat(Integer idAchat) {
		this.idAchat = idAchat;
	}

	/**
	 * 
	 * @return nomClient
	 */
	public String getNomClient() {
		return nomClient;
	}

	/**
	 * 
	 * @param nomClient
	 */
	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	/**
	 * 
	 * @return prénom client
	 */
	public String getPrenomClient() {
		return prenomClient;
	}

	/**
	 * 
	 * @param prenomClient
	 */
	public void setPrenomClient(String prenomClient) {
		this.prenomClient = prenomClient;
	}

	/**
	 * 
	 * @return N° de sécurité sociale
	 */
	public String getSecu() {
		return secu;
	}

	/**
	 * 
	 * @param secu
	 */
	public void setSecu(String secu) {
		this.secu = secu;
	}

	/**
	 * 
	 * @return Medecin
	 */
	public String getMedecin() {
		return medecin;
	}

	/**
	 * 
	 * @param medecin
	 */
	public void setMedecin(String medecin) {
		this.medecin = medecin;
	}

	/**
	 * 
	 * @return Date d'achat
	 */
	public String getDateAchat() {
		return dateAchat;
	}

	/**
	 * 
	 * @param dateAchat
	 */
	public void setDateAchat(String dateAchat) {
		this.dateAchat = dateAchat;
	}

	/**
	 * 
	 * @return spécialiste
	 */
	public String getSpecialiste() {
		return specialiste;
	}

	/**
	 * 
	 * @param specialiste
	 */
	public void setSpecialiste(String specialiste) {
		this.specialiste = specialiste;
	}

	/**
	 * 
	 * @return Médicament
	 */
	public Medicament getMedicament() {
		return medicament;
	}

	/**
	 * 
	 * @param medicament
	 */
	public void setMedicament(Medicament medicament) {
		this.medicament = medicament;
	}

	/**
	 * 
	 * @return Mutuelle
	 */
	public String getMutuelle() {
		return mutuelle;
	}

	/**
	 * 
	 * @param mutuelle
	 */
	public void setMutuelle(String mutuelle) {
		this.mutuelle = mutuelle;
	}

	/**
	 * 
	 * @return quantité des médicaments
	 */
	public Integer getQuantiteMedicament() {
		return quantiteMedicament;
	}

	/**
	 * 
	 * @param quantiteMedicament
	 */
	public void setQuantiteMedicament(Integer quantiteMedicament) {
		this.quantiteMedicament = quantiteMedicament;
	}

	/**
	 * 
	 * @return le prix total
	 */
	public Double getPrixTotal() {
		return prixTotal;
	}

	/**
	 * 
	 * @param prixTotal
	 */
	public void setPrixTotal(Double prixTotal) {
		this.prixTotal = prixTotal;
	}

	/**
	 * 
	 */
	public String toString() {
		return "Nom: " + nomClient + "\nPrénom: " + prenomClient + "\nN° Sécu: " + secu
				+ "\nMédecin Traitant: " + medecin + "\nDate d'achat: " + dateAchat
				+ "\nSpécialiste: " + specialiste + "\nMédicament: " + medicament + "\nMutuelle: "
				+ mutuelle + "\nQuantité Médicament: " + quantiteMedicament + "\nLe prix total: "
				+ prixTotal;
	}
}
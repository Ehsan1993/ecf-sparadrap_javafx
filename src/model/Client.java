package model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

import exception.MissMatchException;

/**
 * 
 * @author User-06
 *
 */
public class Client extends Personne {

	private LocalDate dateDeNaissance;
	private String numSecuriteSociale;
	private String mutuelle;
	private String medecin;
	private Integer idClient;

	/**
	 * 
	 * @param idClient
	 * @param nom
	 * @param prenom
	 * @param dateNaissanceClient
	 * @param ville
	 * @param codePostal
	 * @param adresse
	 * @param phone
	 * @param mail
	 * @param numSecuriteSociale
	 * @param mutuelle
	 * @param medecinTraitant
	 * @throws MissMatchException
	 */
	public Client(Integer idClient, String nom, String prenom, String dateNaissanceClient,
			String ville, String codePostal, String adresse, String phone, String mail,
			String numSecuriteSociale, String mutuelle, String medecinTraitant)
			throws MissMatchException {
		super(nom, prenom, ville, codePostal, adresse, phone, mail);
		setIdClient(idClient);
		setDateDeNaissance(dateNaissanceClient);
		setNumSecuriteSociale(numSecuriteSociale);
		setMutuelle(mutuelle);
		setMedecinTraitant(medecinTraitant);
	}

	/**
	 * 
	 * @param nom
	 * @param prenom
	 * @param dateNaissanceClient
	 * @param ville
	 * @param codePostal
	 * @param adresse
	 * @param phone
	 * @param mail
	 * @param numSecuriteSociale
	 * @param mutuelle
	 * @param medecinTraitant
	 * @throws MissMatchException
	 */
	public Client(String nom, String prenom, String dateNaissanceClient, String ville,
			String codePostal, String adresse, String phone, String mail, String numSecuriteSociale,
			String mutuelle, String medecinTraitant) throws MissMatchException {
		super(nom, prenom, ville, codePostal, adresse, phone, mail);
		setDateDeNaissance(dateNaissanceClient);
		setNumSecuriteSociale(numSecuriteSociale);
		setMutuelle(mutuelle);
		setMedecinTraitant(medecinTraitant);
	}

	/**
	 * 
	 * @return
	 */
	public Integer getIdClient() {
		return idClient;
	}

	/**
	 * 
	 * @param idClient
	 */
	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	/**
	 * 
	 * @return
	 */
	public String getDateDeNaissance() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return dateDeNaissance.format(formatter);
	}

	/**
	 * 
	 * @param dateDeNaissance
	 * @throws MissMatchException
	 */
	public void setDateDeNaissance(String dateDeNaissance) throws MissMatchException {
		if (dateDeNaissance == null || dateDeNaissance.trim().isEmpty()) {
			throw new NumberFormatException("Veuillez saisir la date de naissance");
		} else if (!Pattern.matches("[1|2][0-9]{3}-[0|1][0-9]-[0-3][0-9]", dateDeNaissance)) {
			throw new MissMatchException("La date n'est pas bonne!!!");
		} else {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate dataNaissance = LocalDate.parse(dateDeNaissance, formatter);
			this.dateDeNaissance = dataNaissance;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String getNumSecuriteSociale() {
		return numSecuriteSociale;
	}

	/**
	 * 
	 * @param numSecuriteSociale
	 * @throws MissMatchException
	 */
	public void setNumSecuriteSociale(String numSecuriteSociale) throws MissMatchException {
		if (numSecuriteSociale == null || numSecuriteSociale.trim().isEmpty()) {
			throw new NumberFormatException("Veuillez saisir le numéro de Sécurité Sociale!");
		}
		if (!Pattern.matches("[1|2][0-9]{14}", numSecuriteSociale)) {
			throw new MissMatchException(
					"Un numero de securite sociale contient forcement 15 chiffres et commence par 1 ou 2");

		} else {
			this.numSecuriteSociale = numSecuriteSociale;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String getMutuelle() {
		return mutuelle;
	}

	/**
	 * 
	 * @param mutuelleString
	 */
	public void setMutuelle(String mutuelleString) {
		this.mutuelle = mutuelleString;
	}

	/**
	 * 
	 * @return
	 */
	public String getMedecinTraitant() {
		return medecin;
	}

	/**
	 * 
	 * @param medecinTraitant
	 */
	public void setMedecinTraitant(String medecinTraitant) {
		this.medecin = medecinTraitant;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return this.getNom() + " " + this.getPrenom();
	}
}

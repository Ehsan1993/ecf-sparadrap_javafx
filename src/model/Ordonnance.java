package model;

/**
 * 
 * @author User-06
 *
 */
public class Ordonnance {

	private String client;
	private String dateOrdonnance;
	private Integer idAchat;
	private String nomSpecialiste;
	private String listeMedicament;

	/**
	 * 
	 * @param client
	 * @param dateOrdonnance
	 * @param idAchat
	 * @param nomSpecialiste
	 * @param listeMedicament
	 */
	public Ordonnance(String client, String dateOrdonnance, Integer idAchat, String nomSpecialiste,
			String listeMedicament) {
		setClient(client);
		setDateOrdonnance(dateOrdonnance);
		setIdAchat(idAchat);
		setNomSpecialiste(nomSpecialiste);
		setListeMedicament(listeMedicament);
	}

	/**
	 * 
	 * @return
	 */
	public String getListeMedicament() {
		return listeMedicament;
	}

	/**
	 * 
	 * @param listeMedicament
	 */
	public void setListeMedicament(String listeMedicament) {
		this.listeMedicament = listeMedicament;
	}

	/**
	 * 
	 * @return
	 */
	public String getClient() {
		return client;
	}

	/**
	 * 
	 * @param client
	 */
	public void setClient(String client) {
		this.client = client;
	}

	/**
	 * 
	 * @return
	 */
	public String getDateOrdonnance() {
		return dateOrdonnance;
	}

	/**
	 * 
	 * @param dateOrdonnance
	 */
	public void setDateOrdonnance(String dateOrdonnance) {
		this.dateOrdonnance = dateOrdonnance;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getIdAchat() {
		return idAchat;
	}

	/**
	 * 
	 * @param idAchat
	 */
	public void setIdAchat(Integer idAchat) {
		this.idAchat = idAchat;
	}

	/**
	 * 
	 * @return
	 */
	public String getNomSpecialiste() {
		return nomSpecialiste;
	}

	/**
	 * 
	 * @param nomSpecialiste
	 */
	public void setNomSpecialiste(String nomSpecialiste) {
		this.nomSpecialiste = nomSpecialiste;
	}
}

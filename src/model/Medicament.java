package model;

import java.time.LocalDate;
import java.util.regex.Pattern;

import exception.MissMatchException;

/**
 * 
 * @author User-06
 *
 */
public class Medicament {

	private Integer idMedicament;
	private String nomMedicament;
	private String categorie;
	private Double prix;
	private LocalDate dateService;
	private Integer quantite;

	/**
	 * 
	 * @param idMedicament
	 * @param nomMedicament
	 * @param categorie
	 * @param dateService
	 * @param prix
	 * @param quantite
	 * @throws MissMatchException
	 */
	public Medicament(Integer idMedicament, String nomMedicament, String categorie,
			String dateService, String prix, String quantite) throws MissMatchException {

		setIdMedicament(idMedicament);
		setNom(nomMedicament);
		setCategorie(categorie);
		setPrix(prix);
		setDateService(dateService);
		setQuantite(quantite);

	}

	/**
	 * 
	 * @return
	 */
	public Integer getIdMedicament() {
		return idMedicament;
	}

	/**
	 * 
	 * @param idMedicament
	 */
	public void setIdMedicament(Integer idMedicament) {
		this.idMedicament = idMedicament;
	}

	/**
	 * 
	 * @return
	 */
	public String getNom() {
		return nomMedicament;
	}

	/**
	 * 
	 * @param nom
	 * @throws MissMatchException
	 */
	public void setNom(String nom) throws MissMatchException {
		if (!Pattern.matches("([a-zA-Z]+[-'éèçàîêôï]*)+", nom))
			throw new MissMatchException("Le nom médicament n'est pas bon. Insérez un nom valide!");
		this.nomMedicament = nom;
	}

	/**
	 * 
	 * @return
	 */
	public String getCategorie() {
		return categorie;
	}

	/**
	 * 
	 * @param categorie
	 * @throws MissMatchException
	 */
	public void setCategorie(String categorie) throws MissMatchException {
		if (!Pattern.matches("([a-zA-Z]+[-'éèçàîêôï]*)+", categorie))
			throw new MissMatchException(
					"la catagorie de médicament pas bonne. Insérez un nom valide!");
		this.categorie = categorie;
	}

	/**
	 * 
	 * @return
	 */
	public Double getPrix() {
		return prix;
	}

	/**
	 * 
	 * @param prix
	 * @throws MissMatchException
	 */
	public void setPrix(String prix) throws MissMatchException {
		if (!Pattern.matches("[1-9][0-9]*\\.[0-9]{1,2}", prix))
			throw new MissMatchException("Le prix n'est pas bon. Insérez un prix valide!");
		this.prix = Double.parseDouble(prix);
	}

	/**
	 * 
	 * @return
	 */
	public LocalDate getDateService() {
		return dateService;
	}

	/**
	 * 
	 * @param dateService
	 */
	public void setDateService(String dateService) {
		LocalDate date = LocalDate.parse(dateService);
		this.dateService = date;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getQuantite() {
		return quantite;
	}

	/**
	 * 
	 * @param quantite
	 * @throws MissMatchException
	 */
	public void setQuantite(String quantite) throws MissMatchException {
		if (!Pattern.matches("[1-9][0-9]*{0,2}", quantite))
			throw new MissMatchException(
					"La Quanité n'est pas bonne. Insérez une quantité valide!");
		this.quantite = Integer.parseInt(quantite);
	}

	/**
	 * 
	 */
	public String toString() {
		return this.getNom();
	}
}

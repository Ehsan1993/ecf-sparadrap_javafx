package model;

import java.util.regex.Pattern;

import exception.MissMatchException;

/**
 * 
 * @author User-06
 *
 */
public class Specialiste extends Personne {

	private String specialite;
	private Integer idSpecialiste;

	/**
	 * 
	 * @param idSpecialiste
	 * @param nom
	 * @param prenom
	 * @param ville
	 * @param codePostal
	 * @param adresse
	 * @param phone
	 * @param mail
	 * @param specialite
	 * @throws MissMatchException
	 */
	public Specialiste(Integer idSpecialiste, String nom, String prenom, String ville,
			String codePostal, String adresse, String phone, String mail, String specialite)
			throws MissMatchException {
		super(nom, prenom, ville, codePostal, adresse, phone, mail);
		setSpecialite(specialite);
		setIdSpecialiste(idSpecialiste);
	}

	/**
	 * 
	 * @return
	 */
	public Integer getIdSpecialiste() {
		return idSpecialiste;
	}

	/**
	 * 
	 * @param idSpecialiste
	 */
	public void setIdSpecialiste(Integer idSpecialiste) {
		this.idSpecialiste = idSpecialiste;
	}

	/**
	 * 
	 * @return
	 */
	public String getSpecialite() {
		return specialite;
	}

	/**
	 * 
	 * @param specialite
	 * @throws MissMatchException
	 */
	private void setSpecialite(String specialite) throws MissMatchException {
		if (!Pattern.matches("([a-zA-Z]+[-'éèçàâîêôï]*)+", specialite)) {
			throw new MissMatchException("La specialité n'est pas bonne!");
		} else {
			this.specialite = specialite;
		}
	}

	/**
	 * 
	 */
	public String toString() {
		return "Dr. " + this.getNom() + " " + this.getPrenom();
	}
}

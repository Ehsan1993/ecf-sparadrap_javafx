package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import DAO.AchatDAO;
import DAO.ClientDAO;
import DAO.MedecinDAO;
import DAO.MedicamentDAO;
import DAO.MutuelleDAO;
import DAO.OrdonnanceDAO;
import DAO.SpecialisteDAO;
import exception.MissMatchException;
import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Achat;
import model.Client;
import model.Medecin;
import model.Medicament;
import model.Mutuelle;
import model.Ordonnance;
import model.Specialiste;
import services.Methodes;

/**
 * 
 * @author User-06
 *
 */
public class Controller implements Initializable {

	@FXML
	private Button buttonListeClientPA, buttonAchatPA, enregistrerClient, effacerClient,
			buttonValider, buttonAjouter, buttonSupprimer, buttonBack, listeClient;
	@FXML
	private TextField tfNom, tfPrenom, tfVille, tfCodePostal, tfAdresse, tfPhone, tfMail,
			tfSecuAchat, tfSecuriteSociale;
	@FXML
	private Label labelPrincipale, labelQuantite, labelNom, labelMedicament, labelSpecialiste,
			labelMutuelle, labelSecurite, titreAchatOrdo, titreAchatSansOrdo, labelTypeAchat,
			dateJour, dateJour1, dateJour2, dateJour3;
	@FXML
	private DatePicker datePicker;
	@FXML
	private ComboBox<String> comboBoxTypeAchat;
	@FXML
	private ComboBox<Medicament> comboMedicament;
	@FXML
	private ComboBox<Specialiste> comboSpecialiste;
	@FXML
	private ComboBox<Mutuelle> comboMutuelleAchat;
	@FXML
	private ComboBox<Mutuelle> choiceBoxMutuelle;
	@FXML
	private ComboBox<Medecin> choiceBoxTraitant;
	@FXML
	private ComboBox<Client> comboNom;
	@FXML
	private ComboBox<Medecin> comboMedecin2;
	@FXML
	private Spinner<Integer> spinnerQuantite;
	private SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(
			1, 100);

	@FXML
	private Rectangle animation;
	@FXML
	private TableView<Ordonnance> table_Ordonnance;
	@FXML
	private TableColumn<Ordonnance, String> patient;
	@FXML
	private TableColumn<Ordonnance, String> dateOrdonnance;
	@FXML
	private TableColumn<Ordonnance, String> nomSpecialiste;
	@FXML
	private TableColumn<Ordonnance, String> listeMedicament;
	@FXML
	private TableView<Achat> tableAchat;
	@FXML
	private TableColumn<Achat, String> nomMedicamentAchat;
	@FXML
	private TableColumn<Achat, Double> prixMedicamentAchat;
	@FXML
	private TableColumn<Achat, Double> remboursementMedicament;
	@FXML
	private TableColumn<Achat, Integer> quantiteMedicamentAchat;
	@FXML
	private TableColumn<Achat, Double> totalAchat;
	@FXML
	private ObservableList<Achat> listeAchat = FXCollections.observableArrayList();

	@FXML
	private AnchorPane monPanel;
	private Stage stage;
	private Scene scene;
	private Parent root;

	Connection con;

	/**
	 * 
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		datePicker.getEditor().setDisable(true);
		// Remplir le comboBox Mutuelle de SQL
		MutuelleDAO.recupererMutuelle().forEach(mut -> choiceBoxMutuelle.getItems().add(mut));

		// Remplir le comboBox de Medecin de la page de client
		MedecinDAO.recupererMedecin().forEach(medecin -> choiceBoxTraitant.getItems().add(medecin));

		// Remplir le comboBox de Medecin de la page d'ordonnance
		MedecinDAO.recupererMedecin().forEach(medecin -> comboMedecin2.getItems().add(medecin));

		// Remplir le comboBox de client de SQL
		ClientDAO.recupererClient().forEach(client -> comboNom.getItems().add(client));

		// Remplir le comboBox de Medicament de SQL
		MedicamentDAO.recupererMedicament().forEach(medic -> comboMedicament.getItems().add(medic));

		// Remplir le comboBox de Specialiste de SQL
		SpecialisteDAO.recupererSpecialiste().forEach(spe -> comboSpecialiste.getItems().add(spe));

		// Définir la date de jour
		LocalDate today = LocalDate.now();
		String formattedDate = today.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy"));
		dateJour.setText(formattedDate.toString());
		dateJour1.setText(formattedDate.toString());
		dateJour2.setText(formattedDate.toString());
		dateJour3.setText(formattedDate.toString());

		// Animation du titre d'application "Sparadrap"
		FadeTransition fade = new FadeTransition();
		fade.setNode(labelPrincipale);
		fade.setDuration(Duration.millis(15000));
		fade.setFromValue(0);
		fade.setToValue(1);
		fade.play();

		// Animation derrière l'application
		TranslateTransition translate = new TranslateTransition();
		translate.setNode(animation);
		translate.setDuration(Duration.millis(10000));
		translate.setCycleCount(TranslateTransition.INDEFINITE);
		translate.setAutoReverse(true);
		translate.setByX(1200);
		translate.play();

		// Remplir le combo box type d'achat
		comboBoxTypeAchat.getSelectionModel().select(" ");
		comboBoxTypeAchat.getItems().add("Via Ordonnance");
		comboBoxTypeAchat.getItems().add("Sans Ordonnance");

		// Remplir le spinner pour la quantité des médicaments
		valueFactory.setValue(1);
		spinnerQuantite.setValueFactory(valueFactory);
	}

	/**
	 * 
	 */
	public void typeAchat() {
		if (comboBoxTypeAchat.getValue() != null
				&& comboBoxTypeAchat.getValue().equals("Via Ordonnance")) {

			tableAchat.setVisible(true);
			comboNom.setVisible(true);
			comboMedicament.setVisible(true);
			comboSpecialiste.setVisible(true);
			labelQuantite.setVisible(true);
			labelNom.setVisible(true);
			labelMedicament.setVisible(true);
			labelSpecialiste.setVisible(true);
			labelMutuelle.setVisible(true);
			labelSecurite.setVisible(true);
			tfSecuAchat.setVisible(true);
			comboMutuelleAchat.setVisible(true);
			spinnerQuantite.setVisible(true);
			titreAchatOrdo.setVisible(true);
			buttonValider.setVisible(true);
			buttonAjouter.setVisible(true);
			buttonSupprimer.setVisible(true);
			buttonBack.setVisible(true);
			comboBoxTypeAchat.setVisible(false);
			labelTypeAchat.setVisible(false);

		} else if (comboBoxTypeAchat.getValue() != null
				&& comboBoxTypeAchat.getValue().equals("Sans Ordonnance")) {

			tableAchat.setVisible(true);
			comboNom.setVisible(true);
			comboMedicament.setVisible(true);
			labelQuantite.setVisible(true);
			labelNom.setVisible(true);
			labelMedicament.setVisible(true);
			labelSecurite.setVisible(true);
			tfSecuAchat.setVisible(true);
			spinnerQuantite.setVisible(true);
			titreAchatSansOrdo.setVisible(true);
			buttonValider.setVisible(true);
			buttonAjouter.setVisible(true);
			buttonSupprimer.setVisible(true);
			buttonBack.setVisible(true);

			comboSpecialiste.setVisible(false);
			labelSpecialiste.setVisible(false);
			labelMutuelle.setVisible(false);
			comboMutuelleAchat.setVisible(false);
			comboBoxTypeAchat.setVisible(false);
			labelTypeAchat.setVisible(false);
		}
	}

	/**
	 * 
	 */
	public void comboNom() {

		Client clientCombo = comboNom.getValue();
		if (clientCombo != null) {
			tfSecuAchat.setText(clientCombo.getNumSecuriteSociale());
			String mutuelleNom = clientCombo.getMutuelle();

			comboMutuelleAchat.getItems().clear();

			// Remplir le combo box mutuelle par rapport à la client à la page d'achat

			MutuelleDAO.recupererMutuelle_Achat(mutuelleNom)
					.forEach(mut -> comboMutuelleAchat.getItems().add(mut));
		}
	}

	/**
	 * 
	 */
	public void buttonBack() {

		tableAchat.setVisible(false);
		comboNom.setVisible(false);
		comboMedicament.setVisible(false);
		comboSpecialiste.setVisible(false);
		labelQuantite.setVisible(false);
		labelNom.setVisible(false);
		labelMedicament.setVisible(false);
		labelSpecialiste.setVisible(false);
		labelMutuelle.setVisible(false);
		labelSecurite.setVisible(false);
		tfSecuAchat.setVisible(false);
		comboMutuelleAchat.setVisible(false);
		spinnerQuantite.setVisible(false);
		titreAchatOrdo.setVisible(false);
		titreAchatSansOrdo.setVisible(false);
		buttonValider.setVisible(false);
		buttonAjouter.setVisible(false);
		buttonSupprimer.setVisible(false);
		buttonBack.setVisible(false);

		comboNom.getSelectionModel().clearSelection();
		comboSpecialiste.getSelectionModel().clearSelection();
		comboMedicament.getSelectionModel().clearSelection();
		comboMutuelleAchat.getItems().clear();
		comboBoxTypeAchat.getSelectionModel().clearSelection();
		tfSecuAchat.setText(null);
		valueFactory.setValue(1);
		comboBoxTypeAchat.setVisible(true);
		labelTypeAchat.setVisible(true);
		listeAchat.clear();
	}

	/**
	 * 
	 * @param event button Quitter
	 */
	public void buttonQuitter(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Quitter");
		alert.setHeaderText("Vous êtes en train de sortir du programme!");
		alert.setContentText("Vous êtes sûre de vouloir quitter?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			stage = (Stage) monPanel.getScene().getWindow();
			stage.close();
		}
	}

	/**
	 * 
	 */
	public void buttonListeClientPA() {
		buttonListeClientPA.toFront();
		buttonListeClientPA.setPrefSize(150, 150);
		buttonListeClientPA.relocate(75, 25);
		buttonListeClientPA.setText("Liste des clients");
		buttonListeClientPA.setFont(new Font("Times New Roman", 18));
	}

	/**
	 * 
	 */
	public void buttonAchatPA() {
		buttonAchatPA.toFront();
		buttonAchatPA.setPrefSize(150, 150);
		buttonAchatPA.relocate(175, 25);
		buttonAchatPA.setText("Liste d'achat");
		buttonAchatPA.setFont(new Font("Times New Roman", 18));
	}

	/**
	 * 
	 */
	public void buttonExit() {

		buttonListeClientPA.setFont(new Font("Times New Roman", 12));
		buttonListeClientPA.setPrefSize(100, 100);
		buttonAchatPA.setFont(new Font("Times New Roman", 12));
		buttonAchatPA.setPrefSize(100, 100);
		buttonListeClientPA.setText("Liste client");
		buttonListeClientPA.relocate(100, 50);
		buttonAchatPA.setText("Achat");
		buttonAchatPA.relocate(200, 50);
	}

	/**
	 * 
	 * @param event Pour aller à la page qui contient la liste d'achat
	 * @throws IOException - IO
	 */
	public void listeAchat(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/view/FenetreListeAchat.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * 
	 * @param event Pour aller à la page qui contient la liste des clients
	 * @throws IOException - IO
	 */
	public void listeClient(ActionEvent event) throws IOException {
		try {
			root = FXMLLoader.load(getClass().getResource("/view/FenetreListeClient.fxml"));
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur");
			alert.setHeaderText("Problème technique!!");
			alert.setContentText("Veuillez appeler la personne concernée!!");
			alert.showAndWait();
		}
	}

	/**
	 * Limiter le nombre des chiffres dans la case de sécurité sociale
	 */
	public void numSecu() {
		Methodes.RestrictionTextField(tfSecuriteSociale, 15, "numéro de Sécurité Sociale");
	}

	/**
	 * Limiter le nombre des chiffres dans la case de téléphone
	 */
	public void numTel() {
		Methodes.RestrictionTextField(tfPhone, 10, "numéro de téléphone");
	}

	/**
	 * Limiter le nombre des chiffres dans la case de code postale
	 */
	public void codePostal() {
		Methodes.RestrictionTextField(tfCodePostal, 5, "code postale");
	}

	/**
	 * Proces de l'enregistrement d'un nouveau client
	 */
	public void buttonEnregistrer() {

		try {
			String nom = tfNom.getText();
			String prenom = tfPrenom.getText();

			LocalDate date = datePicker.getValue();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String naissance = date.format(formatter);

			String ville = tfVille.getText();
			String codePostal = tfCodePostal.getText();
			String adresse = tfAdresse.getText();
			String phone = tfPhone.getText();
			String mail = tfMail.getText();
			String secu = tfSecuriteSociale.getText();
			Mutuelle mutuelle = choiceBoxMutuelle.getValue();
			Medecin medecin = choiceBoxTraitant.getValue();

			Client client = new Client(nom, prenom, naissance, ville, codePostal, adresse, phone,
					mail, secu, String.valueOf(mutuelle.getId_Mutuelle()),
					String.valueOf(medecin.getId_Medecin()));

			ClientDAO.enregistrerClient(client);
			buttonEffacer();
		} catch (MissMatchException e2) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur");
			alert.setHeaderText("Erreur de Format");
			alert.setContentText(e2.getMessage());
			alert.showAndWait();
		} catch (NumberFormatException e1) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur");
			alert.setHeaderText("Erreur de Format");
			alert.setContentText(e1.getMessage());
			alert.showAndWait();
		} catch (NullPointerException NPE) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur");
			alert.setHeaderText("Manque d'information!!!");
			alert.setContentText("Veuillez remplir les champs!!!");
			alert.showAndWait();
		}

	}

	/**
	 * Ajouter des médicaments dans le panier
	 */
	public void buttonAjouterMedicament() {
		try {
			Client client = comboNom.getValue();
			Medicament medicament = comboMedicament.getValue();
			Specialiste specialiste = comboSpecialiste.getValue();
			Integer quantiteMedic = spinnerQuantite.getValue();
			Double prixMedic = medicament.getPrix();
			LocalDate today = LocalDate.now();
			String dateAchat = today.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

			if (comboBoxTypeAchat.getValue().equals("Sans Ordonnance")) {

				// Objet l'achat pour la liste d'Achat
				listeAchat.add(new Achat(client.getNom(), client.getPrenom(),
						client.getNumSecuriteSociale(), client.getMedecinTraitant(), dateAchat,
						null, medicament, null, quantiteMedic, prixMedic, 0.0,
						prixMedic * quantiteMedic));

			} else if (comboBoxTypeAchat.getValue().equals("Via Ordonnance")) {

				Mutuelle mutuelle = comboMutuelleAchat.getValue();
				Double rembourse = mutuelle.getRemboursement();
				Double prixTotalOrdonnance = (prixMedic - ((rembourse / 100) * prixMedic))
						* quantiteMedic;
				Double prixTot = Math.round(prixTotalOrdonnance * 100.0) / 100.0;

				if (comboSpecialiste.getValue() == null) {
					// Objet l'achat pour la liste d'Achat
					listeAchat.add(new Achat(client.getNom(), client.getPrenom(),
							client.getNumSecuriteSociale(), client.getMedecinTraitant(), dateAchat,
							null, medicament, mutuelle.getNom(), quantiteMedic, prixMedic,
							rembourse, prixTot));
				} else {
					// Objet l'achat pour la liste d'Achat
					listeAchat.add(new Achat(client.getNom(), client.getPrenom(),
							client.getNumSecuriteSociale(), client.getMedecinTraitant(), dateAchat,
							specialiste.getNom(), medicament, mutuelle.getNom(), quantiteMedic,
							prixMedic, rembourse, prixTot));
				}
			}

			nomMedicamentAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("medicament"));
			prixMedicamentAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, Double>("prix"));
			remboursementMedicament
					.setCellValueFactory(new PropertyValueFactory<Achat, Double>("remboursement"));
			quantiteMedicamentAchat.setCellValueFactory(
					new PropertyValueFactory<Achat, Integer>("quantiteMedicament"));
			totalAchat.setCellValueFactory(new PropertyValueFactory<Achat, Double>("prixTotal"));

			tableAchat.setItems(listeAchat);
		} catch (NullPointerException npe) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("ERROR");
			alert.setHeaderText("Manque d'information!");
			alert.setContentText("Veuillez remplir les champs ?");
			alert.showAndWait();
		}
		comboMedicament.getSelectionModel().clearSelection();
		valueFactory.setValue(1);
	}

	/**
	 * Supprimer le médicament du panier
	 */
	public void buttonSupprimer() {
		try {
			Achat achatSupprimé = tableAchat.getSelectionModel().getSelectedItem();
			tableAchat.getItems().remove(achatSupprimé);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("ERREUR");
			alert.setHeaderText("Manque d'information!");
			alert.setContentText("Veuillez remplir le tableau d'abord ?");
			alert.showAndWait();
		}
	}

	/**
	 * 
	 */
	public void buttonValiderAchat() {

		ObservableList<Achat> items = tableAchat.getItems();

		if (items.size() > 0) {
			Client client = comboNom.getValue();
			LocalDate today = LocalDate.now();
			String dateAchat = today.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

			// Enregistrer l'achat dans la liste d'achat à la base de donnée

			if (comboBoxTypeAchat.getValue().equals("Sans Ordonnance")) {
				AchatDAO.EnregistrerAchat(client.getIdClient(), dateAchat, null);
			} else if (comboBoxTypeAchat.getValue().equals("Via Ordonnance")) {
				AchatDAO.EnregistrerAchat(client.getIdClient(), dateAchat,
						comboMutuelleAchat.getValue().getId_Mutuelle());
			}

			// Récupérer id_Achat de la base de donnée
			Integer idAchat = AchatDAO.recupererIdAchat(client.getIdClient()).get(0);

			// Remplir la table Ordonnance en base de donnée
			if (comboBoxTypeAchat.getValue().equals("Via Ordonnance")) {
				if (comboSpecialiste.getValue() != null) {
					AchatDAO.EnregistrerOrdonnance(dateAchat, idAchat,
							comboSpecialiste.getValue().getIdSpecialiste());
				} else {
					AchatDAO.EnregistrerOrdonnance(dateAchat, idAchat, null);
				}
			}

			// Remplir la liste médicament en base de donnée
			for (Achat achat : items) {

				dateAchat = achat.getDateAchat();
				Integer quantite = achat.getQuantiteMedicament();
				Integer idMedi = achat.getMedicament().getIdMedicament();
				AchatDAO.enregistrerMedicamentAchete(idAchat, idMedi, quantite);
			}
			items.clear();
			comboMedicament.getSelectionModel().clearSelection();
			comboNom.getSelectionModel().clearSelection();
			comboMutuelleAchat.getSelectionModel().clearSelection();
			comboSpecialiste.getSelectionModel().clearSelection();
			tfSecuAchat.setText(null);
			valueFactory.setValue(1);
			tableAchat.getItems().clear();
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("ERREUR");
			alert.setHeaderText("Manque d'information!");
			alert.setContentText("Veuillez choisir les médicaments d'abord ?");
			alert.showAndWait();
		}
	}

	/**
	 * Vider le champs après l'achat
	 */
	public void buttonEffacer() {

		tfNom.setText("");
		tfPrenom.setText("");
		tfVille.setText("");
		tfCodePostal.setText("");
		tfPhone.setText("");
		tfMail.setText("");
		tfSecuriteSociale.setText("");
		tfAdresse.setText("");
		datePicker.setValue(null);
		choiceBoxMutuelle.setValue(null);
		choiceBoxTraitant.setValue(null);
	}

	/**
	 * 
	 */
	public void listeOrdonnance() {
		table_Ordonnance.getItems().clear();

		// Récupérer les ordonnances de la BDD
		OrdonnanceDAO.recupererOrdonnance().forEach(ordo -> table_Ordonnance.getItems().add(ordo));

		// Saisir les données dans le tableau d'affichage
		patient.setCellValueFactory(new PropertyValueFactory<Ordonnance, String>("client"));
		dateOrdonnance.setCellValueFactory(
				new PropertyValueFactory<Ordonnance, String>("dateOrdonnance"));
		nomSpecialiste.setCellValueFactory(
				new PropertyValueFactory<Ordonnance, String>("nomSpecialiste"));
		listeMedicament.setCellValueFactory(
				new PropertyValueFactory<Ordonnance, String>("listeMedicament"));
	}

	/**
	 * 
	 */
	public void listeOrdonnanceParMedecin() {
		try {
			table_Ordonnance.getItems().clear();
			Integer idMedecin = comboMedecin2.getValue().getId_Medecin();

			OrdonnanceDAO.recupererOrdoParMedecin(idMedecin)
					.forEach(ordo -> table_Ordonnance.getItems().add(ordo));

			patient.setCellValueFactory(new PropertyValueFactory<Ordonnance, String>("client"));
			dateOrdonnance.setCellValueFactory(
					new PropertyValueFactory<Ordonnance, String>("dateOrdonnance"));
			nomSpecialiste.setCellValueFactory(
					new PropertyValueFactory<Ordonnance, String>("nomSpecialiste"));
			listeMedicament.setCellValueFactory(
					new PropertyValueFactory<Ordonnance, String>("listeMedicament"));
		} catch (NullPointerException NPE) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("ERREUR");
			alert.setHeaderText("Manque d'information!");
			alert.setContentText("Veuillez choisir un médecin!!!");
			alert.showAndWait();
		}
	}
}
package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import DAO.AchatDAO;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Achat;
import model.Medicament;

/**
 * 
 * @author User-06
 *
 */
public class Controller_ListeAchat implements Initializable {

	@FXML
	private Rectangle rectangleAnimation;

	@FXML
	private TableView<Achat> tableListeAchat;
	@FXML
	private TableColumn<Achat, String> dateAchat;
	@FXML
	private TableColumn<Achat, String> nomClientAchat;
	@FXML
	private TableColumn<Achat, String> prenomAchat;
	@FXML
	private TableColumn<Achat, String> secuAchat;
	@FXML
	private TableColumn<Achat, String> traitantAchat;
	@FXML
	private TableColumn<Achat, String> specialisteAchat;
	@FXML
	private TableColumn<Achat, String> mutuelleAchat;
	@FXML
	private TableColumn<Achat, Medicament> medicamentAchat;
	@FXML
	private TableColumn<Achat, Integer> quantiteAchat;
	@FXML
	private TableColumn<Achat, Double> totalAchat;

	@FXML
	private RadioButton radioListeAchatJour;
	@FXML
	private RadioButton radioListeAchat;

	@FXML
	private AnchorPane monPanel;
	private Stage stage;
	private Scene scene;
	private Parent root;

	/**
	 * 
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		ToggleGroup tg = new ToggleGroup();
		radioListeAchat.setToggleGroup(tg);
		radioListeAchatJour.setToggleGroup(tg);

		TranslateTransition translate = new TranslateTransition();
		translate.setNode(rectangleAnimation);
		translate.setDuration(Duration.millis(8000));
		translate.setCycleCount(TranslateTransition.INDEFINITE);
		translate.setByX(1300);
		translate.play();
	}

	/**
	 * Afficher la liste des achats par jour ou complet
	 */
	public void radiobutton() {

		if (radioListeAchat.isSelected()) {
			tableListeAchat.getItems().clear();

			// Récupérer les données de la BDD
			AchatDAO.recupererlisteAchat().forEach(achat -> tableListeAchat.getItems().add(achat));

			// Saisir les données dans le tableau
			dateAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("dateAchat"));
			nomClientAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("nomClient"));
			prenomAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("prenomClient"));
			secuAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("secu"));
			traitantAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("medecin"));
			mutuelleAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("mutuelle"));
			medicamentAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, Medicament>("medicament"));
			quantiteAchat.setCellValueFactory(
					new PropertyValueFactory<Achat, Integer>("quantiteMedicament"));
			totalAchat.setCellValueFactory(new PropertyValueFactory<Achat, Double>("prixTotal"));
			specialisteAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("specialiste"));

		} else if (radioListeAchatJour.isSelected()) {
			tableListeAchat.getItems().clear();

			// Récupérer les données de la BDD
			AchatDAO.recupererlisteAchat_Jour()
					.forEach(achat -> tableListeAchat.getItems().add(achat));

			// Saisir les données dans le tableau
			dateAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("dateAchat"));
			nomClientAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("nomClient"));
			prenomAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("prenomClient"));
			secuAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("secu"));
			traitantAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("medecin"));
			specialisteAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("specialiste"));
			mutuelleAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("mutuelle"));
			medicamentAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, Medicament>("medicament"));
			quantiteAchat.setCellValueFactory(
					new PropertyValueFactory<Achat, Integer>("quantiteMedicament"));
			totalAchat.setCellValueFactory(new PropertyValueFactory<Achat, Double>("prixTotal"));
		}
	}

	/**
	 * Revenir à la page d'accueil
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void pageAccueil(ActionEvent event) throws IOException {

		root = FXMLLoader.load(getClass().getResource("/view/Main.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * 
	 * @param event
	 */
	public void buttonQuitter(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Quitter");
		alert.setHeaderText("Vous êtes en train de sortir du programme!");
		alert.setContentText("Vous êtes sûre de vouloir quitter?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.exit(0);
		}
	}
}
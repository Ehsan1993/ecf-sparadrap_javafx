package controllers;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import DAO.ClientDAO;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Client;

/**
 * 
 * @author User-06
 *
 */
public class Controller_ListeClient implements Initializable {

	@FXML
	private Rectangle rectangleAnimation;

	@FXML
	private TableView<Client> tableListeClient;
	@FXML
	private TableColumn<Client, String> nom;
	@FXML
	private TableColumn<Client, String> prenom;
	@FXML
	private TableColumn<Client, LocalDate> naissance;
	@FXML
	private TableColumn<Client, String> ville;
	@FXML
	private TableColumn<Client, String> postal;
	@FXML
	private TableColumn<Client, String> adresse;
	@FXML
	private TableColumn<Client, String> phone;
	@FXML
	private TableColumn<Client, String> mail;
	@FXML
	private TableColumn<Client, String> secu;
	@FXML
	private TableColumn<Client, String> mutuelle;
	@FXML
	private TableColumn<Client, String> traitant;

	@FXML
	private AnchorPane monPanel;
	private Stage stage;
	private Scene scene;
	private Parent root;

	/**
	 * 
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		TranslateTransition translate = new TranslateTransition();
		translate.setNode(rectangleAnimation);
		translate.setDuration(Duration.millis(8000));
		translate.setCycleCount(TranslateTransition.INDEFINITE);
		translate.setByX(1300);
		translate.play();

		// Récupérer les clients de la BDD pour remplir le tableau

		nom.setCellValueFactory(new PropertyValueFactory<Client, String>("nom"));
		prenom.setCellValueFactory(new PropertyValueFactory<Client, String>("prenom"));
		naissance.setCellValueFactory(
				new PropertyValueFactory<Client, LocalDate>("dateDeNaissance"));
		ville.setCellValueFactory(new PropertyValueFactory<Client, String>("ville"));
		postal.setCellValueFactory(new PropertyValueFactory<Client, String>("codePostal"));
		adresse.setCellValueFactory(new PropertyValueFactory<Client, String>("adresse"));
		phone.setCellValueFactory(new PropertyValueFactory<Client, String>("phone"));
		mail.setCellValueFactory(new PropertyValueFactory<Client, String>("mail"));
		secu.setCellValueFactory(new PropertyValueFactory<Client, String>("numSecuriteSociale"));
		mutuelle.setCellValueFactory(new PropertyValueFactory<Client, String>("mutuelle"));
		traitant.setCellValueFactory(new PropertyValueFactory<Client, String>("medecinTraitant"));

		ClientDAO.recupererClient().forEach(client -> tableListeClient.getItems().add(client));
	}

	/**
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void pageAccueil(ActionEvent event) throws IOException {

		root = FXMLLoader.load(getClass().getResource("/view/Main.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * 
	 * @param event
	 */
	public void buttonQuitter(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Quitter");
		alert.setHeaderText("Vous êtes en train de sortir du programme!");
		alert.setContentText("Vous êtes sûre de vouloir quitter?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.exit(0);
		}
	}
}

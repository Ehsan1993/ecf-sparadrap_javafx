module ProjetCFbyJavaFX {

	requires javafx.controls;
	requires javafx.graphics;
	requires javafx.fxml;
	requires javafx.base;
	requires java.sql;
	requires mysql.connector.java;
	requires java.base;
	requires org.junit.platform.suite.api;
	requires org.junit.jupiter.api;

	opens application to javafx.graphics, javafx.fxml;
	opens controllers to javafx.graphics, javafx.fxml;
	opens model to javafx.base;
}

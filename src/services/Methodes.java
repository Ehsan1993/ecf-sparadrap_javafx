package services;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

/**
 * 
 * @author User-06
 *
 */
public class Methodes {
	public static void RestrictionTextField(TextField textField, int length, String nomCase) {

		if (textField.getText().length() < length || textField.getText().trim().isEmpty()
				|| textField.getText() == null) {
			textField.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID,
					CornerRadii.EMPTY, new BorderWidths(2))));
		} else if (textField.getText().length() == length
				&& !textField.getText().trim().isEmpty()) {
			textField.setBorder(null);
		} else if (textField.getText().length() > length) {
			textField.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID,
					CornerRadii.EMPTY, new BorderWidths(2))));
			String text = textField.getText();
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText(null);
			alert.setContentText(
					"Un " + nomCase + " ne peut pas être plus de " + length + " chiffres");
			alert.showAndWait();
			textField.setText(text.substring(0, text.length() - 1));
		}
	}
}

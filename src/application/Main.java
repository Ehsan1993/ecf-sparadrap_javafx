package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

	/**
	 * 
	 */
	@Override
	public void start(Stage stage) throws IOException {

		Parent root = FXMLLoader.load(getClass().getResource("/view/Main.fxml"));
		Scene scene = new Scene(root, 1000, 800);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		Image image = new Image("iconPharmacie.png");
		stage.getIcons().add(image);
		stage.setScene(scene);
		stage.setTitle("Sparadrap");
		stage.setResizable(false);
		stage.show();

		stage.setOnCloseRequest(event -> {
			event.consume();
			buttonQuitter(stage);
		});
	}

	/**
	 * 
	 * @param stage Cette méthode est pour demander la confirmation d'utilistaur de
	 *              quitter après le click sur le bouton rouge d'exit au coin de la
	 *              fenêtre
	 */
	public void buttonQuitter(Stage stage) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Quitter");
		alert.setHeaderText("Vous êtes en train de sortir du programme!");
		alert.setContentText("Vous êtes sûre de vouloir quitter?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			stage.close();
		}
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}

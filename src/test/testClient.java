package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import exception.MissMatchException;
import model.Client;

public class testClient {

	Client client;

	@Test
	public void testConstructor() throws MissMatchException {
		client = new Client(1, "BAYAT", "Ehsan", "1993-01-27", "Maxéville", "54320",
				"11 rue de la seille", "0780248467", "m.ehsanbayat@gmail.com", "125478541254785",
				"CMU", "Philippe");

		assertEquals(1, client.getIdClient());
		assertEquals("BAYAT", client.getNom());
		assertEquals("Ehsan", client.getPrenom());
		assertEquals("1993-01-27", client.getDateDeNaissance());
		assertEquals("Maxéville", client.getVille());
		assertEquals("54320", client.getCodePostal());
		assertEquals("11 rue de la seille", client.getAdresse());
		assertEquals("0780248467", client.getPhone());
		assertEquals("m.ehsanbayat@gmail.com", client.getMail());
		assertEquals("125478541254785", client.getNumSecuriteSociale());
		assertEquals("Philippe", client.getMedecinTraitant());
		assertEquals("CMU", client.getMutuelle());
	}

	@Test
	public void testConstructorSansID() throws MissMatchException {
		client = new Client("BAYAT", "Ehsan", "1993-01-27", "Maxéville", "54320",
				"11 rue de la seille", "0780248467", "m.ehsanbayat@gmail.com", "125478541254785",
				"CMU", "Philippe");

		assertEquals("BAYAT", client.getNom());
		assertEquals("Ehsan", client.getPrenom());
		assertEquals("1993-01-27", client.getDateDeNaissance());
		assertEquals("Maxéville", client.getVille());
		assertEquals("54320", client.getCodePostal());
		assertEquals("11 rue de la seille", client.getAdresse());
		assertEquals("0780248467", client.getPhone());
		assertEquals("m.ehsanbayat@gmail.com", client.getMail());
		assertEquals("125478541254785", client.getNumSecuriteSociale());
		assertEquals("Philippe", client.getMedecinTraitant());
		assertEquals("CMU", client.getMutuelle());
	}

	@Test
	public void testNomFormat() throws MissMatchException {

		try {
			client = new Client("B.ayat", "Ehsan", "11/02/1990", "Maxéville", "54320",
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");

		} catch (MissMatchException e) {
			assert (e.getMessage().contains("Le nom n'est pas bon. Insérez un nom valide!"));
		}
	}

	@Test
	public void testNomNull() throws MissMatchException {
		try {
			client = new Client(null, "Ehsan", "11/02/1990", "Maxéville", "54320",
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un nom!"));
		}
	}

	@Test
	public void testNomEmpty() throws MissMatchException {
		try {
			client = new Client("", "Ehsan", "11/02/1990", "Maxéville", "54320",
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un nom!"));
		}
	}

	@Test
	public void testPrenomFormat() throws MissMatchException {

		try {
			client = new Client("Bayat", "Eh.san", "11/02/1990", "Maxéville", "54320",
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (MissMatchException e) {
			assert (e.getMessage().contains("Le prénom n'est pas bon. Insérez un prénom valide!"));
		}
	}

	@Test
	public void testPrenomNull() throws MissMatchException {
		try {
			client = new Client("Bayat", null, "11/02/1990", "Maxéville", "54320",
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un prénom!"));
		}
	}

	@Test
	public void testPrenomEmpty() throws MissMatchException {
		try {
			client = new Client("Bayat", "", "11/02/1990", "Maxéville", "54320",
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un prénom!"));
		}
	}

	@Test
	public void testNullDateNaissance() throws MissMatchException {
		try {
			client = new Client("BAYAT", "Ehsan", null, "Maxéville", "54320", "11 rue de la seille",
					"0780248467", "m.ehsanbayat@gmail.com", "125478541254785", "Miltis",
					"Philippe");
		} catch (NumberFormatException NFE) {
			assert (NFE.getMessage().contains("Veuillez saisir la date de naissance"));
		}
	}

	@Test
	public void testFormatDateNaissance() throws MissMatchException {
		try {
			client = new Client("BAYAT", "Ehsan", "27-01-1993", "Maxéville", "54320",
					"11 rue de la seille", "0780248467", "m.ehsan@gmail.com", "125478541254785",
					"Miltis", "Philippe");
		} catch (MissMatchException MME) {
			assert (MME.getMessage().contains("Le format de date n'est pas bon!!!"));
		}
	}

	@Test
	public void testVilleFormat() throws MissMatchException {

		try {
			client = new Client("Bayat", "Ehsan", "11/02/1990", "(Maxéville)", "54320",
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("Le nom de ville n'est pas bon. Insérez un nom valide!"));
		}
	}

	@Test
	public void testVilleNull() throws MissMatchException {
		try {
			client = new Client("Bayat", "Ehsan", "11/02/1990", null, "54320",
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir le nom de la ville!"));
		}
	}

	@Test
	public void testVilleEmpty() throws MissMatchException {
		try {
			client = new Client("Bayat", "Ehsan", "11/02/1990", "", "54320", "11 Rue de la Seille",
					"0780248467", "m.ehsanbayat@gmail.com", "122333444455555", "CMU", "Philippe");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir le nom de la ville!"));
		}
	}

	@Test
	public void testCodePostalFormat() throws MissMatchException {

		try {
			client = new Client("Bayat", "Ehsan", "11/02/1990", "Maxéville", "543220",
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (MissMatchException e) {
			assert (e.getMessage().contains(
					"Le code postal n'est pas bon. Un code postal a forcement 5 chiffres!"));
		}
	}

	@Test
	public void testCodePostalNull() throws MissMatchException {
		try {
			client = new Client("Bayat", "Ehsan", "11/02/1990", "Maxéville", null,
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir le code postal!"));
		}
	}

	@Test
	public void testCodePostalEmpty() throws MissMatchException {
		try {
			client = new Client("Bayat", "Ehsan", "11/02/1990", "Maxéville", "",
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir le code postal!"));
		}
	}

	@Test
	public void testPhoneNull() throws MissMatchException {
		try {
			client = new Client("Bayat", "Ehsan", "11/02/1990", "Maxéville", "54320",
					"11 Rue de la Seille", null, "m.ehsanbayat@gmail.com", "122333444455555", "CMU",
					"Philippe");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un numéro de téléphone!"));
		}
	}

	@Test
	public void testPhoneEmpty() throws MissMatchException {
		try {
			client = new Client("Bayat", "Ehsan", "11/02/1990", "Maxéville", "",
					"11 Rue de la Seille", " ", "m.ehsanbayat@gmail.com", "122333444455555", "CMU",
					"Philippe");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un numéro de téléphone!"));
		}
	}

	@Test
	public void testPhoneFormat() throws MissMatchException {

		try {
			client = new Client("Bayat", "Ehsan", "11/02/1990", "Maxéville", "54320",
					"11 Rue de la Seille", "07802248467", "m.ehsanbayat@gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("Le numéro de téléphone n'est pas bon. Insérez un numéro valide!"));
		}
	}

	@Test
	public void testMailNull() throws MissMatchException {
		try {
			client = new Client(1, "Bayat", "Ehsan", "11/02/1990", "Maxéville", "54320",
					"11 Rue de la Seille", "0780224847", null, "122333444455555", "CMU",
					"Philippe");
		} catch (NumberFormatException NFE) {
			assert (NFE.getMessage().contains("Veuillez saisir l'adresse mail!"));
		}
	}

	@Test
	public void testMailEmpty() throws MissMatchException {
		try {
			client = new Client(1, "Bayat", "Ehsan", "11/02/1990", "Maxéville", "54320",
					"11 Rue de la Seille", "0780224847", "   ", "122333444455555", "CMU",
					"Philippe");
		} catch (NumberFormatException NFE) {
			assert (NFE.getMessage().contains("Veuillez saisir l'adresse mail!"));
		}
	}

	@Test
	public void testMailFormat() throws MissMatchException {

		try {
			client = new Client(2, "Bayat", "Ehsan", "11/02/1990", "Maxéville", "54320",
					"11 Rue de la Seille", "0780248467", "m.ehsanbayat.gmail.com",
					"122333444455555", "CMU", "Philippe");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("L'adresse mail n'est pas bon. Insérez une adresse mail valide!"));
		}
	}

	@Test
	public void testNullNumSecu() throws MissMatchException {
		try {
			client = new Client("BAYAT", "Ehsan", "1993-01-27", "Maxéville", "54320",
					"11 rue de la seille", "0780248467", "m.ehsanbayat@gmail.com", null, "CMU",
					"Philippe");
		} catch (NumberFormatException NFE) {
			assert (NFE.getMessage().contains("Veuillez saisir le numéro de Sécurité Sociale!"));
		}
	}

	@Test
	public void testFormatNumSecu() throws MissMatchException {
		try {
			client = new Client(1, "BAYAT", "Ehsan", "1993-01-27", "Maxéville", "54320",
					"11 rue de la seille", "0780248467", "m.bayat@gmail.com", "1245478544125475",
					"Miltis", "Philippe");
		} catch (MissMatchException NFE) {
			assert (NFE.getMessage().contains(
					"Un numero de securite sociale contient forcement 15 chiffres et commence par 1 ou 2"));
		}
	}

	@Test
	public void testToString() throws MissMatchException {
		client = new Client(1, "BAYAT", "Ehsan", "1993-01-27", "Maxéville", "54320",
				"11 rue de la seille", "0780248467", "m.ehsanbayat@gmail.com", "125478544125485",
				"CMU", "Philippe");
		assertEquals("BAYAT" + " " + "Ehsan", client.toString());

	}
}

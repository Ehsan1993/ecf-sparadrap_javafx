package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import exception.MissMatchException;
import model.Mutuelle;

public class testMutuelle {

	Mutuelle mutuelleTest;

	@Test
	public void MissMatchException() throws exception.MissMatchException {
		mutuelleTest = new Mutuelle(1, "Miltis", "Rhône", "Lyon", "69003", "Cr Albert Thomas",
				"0727852788", "mutuelle@miltis.fr", "40.0");
		assertEquals(1, mutuelleTest.getId_Mutuelle());
		assertEquals("Miltis", mutuelleTest.getNom());
		assertEquals("Cr Albert Thomas", mutuelleTest.getAdresse());
		assertEquals("69003", mutuelleTest.getCodePostal());
		assertEquals("Lyon", mutuelleTest.getVille());
		assertEquals("0727852788", mutuelleTest.getPhone());
		assertEquals("mutuelle@miltis.fr", mutuelleTest.getMail());
		assertEquals("Rhône", mutuelleTest.getDepartement());
		assertEquals(40.0, mutuelleTest.getRemboursement());

	}

	@Test
	public void testCodePostal() {
		try {
			mutuelleTest = new Mutuelle(1, "Miltis", "Rhône", "Lyon", "690303", "Cr Albert Thomas",
					"0727852788", "mutuelle@miltis.fr", "40.0");
		} catch (MissMatchException e) {
			assert (e.getMessage().contains(
					"Le code postal n'est pas bon. Un code postal est forcement 5 chiffres!"));
		}
	}

	@Test
	public void testVille() {
		try {
			mutuelleTest = new Mutuelle(1, "Miltis", "Rhône", "Ly.on", "69003", "Cr Albert Thomas",
					"0727852788", "mutuelle@miltis.fr", "40.0");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("Le nom de ville n'est pas bon. Insérez un nom valide!"));
		}
	}

	@Test
	public void testPhone() {
		try {
			mutuelleTest = new Mutuelle(1, "Miltis", "Rhône", "Lyon", "69003", "Cr Albert Thomas",
					"07278523788", "mutuelle@miltis.fr", "40.0");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("Le numéro de téléphone n'est pas bon. Insérez un numéro valide!"));
		}
	}

	@Test
	public void testMail() {
		try {
			mutuelleTest = new Mutuelle(1, "Miltis", "Rhône", "Lyon", "69003", "Cr Albert Thomas",
					"0727852788", "mutuelle.miltis.fr", "40.0");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("L'adresse mail n'est pas bon. Insérez une adresse mail valide!"));
		}
	}

	@Test
	public void testDepartement() {
		try {
			mutuelleTest = new Mutuelle(1, "Miltis", "Rh.ône", "Lyon", "69003", "Cr Albert Thomas",
					"0727852788", "mutuelle@miltis.fr", "40.0");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("Le nom de Département n'est pas bon. Insérez un nom valide!"));
		}
	}

	@Test
	public void testRemboursement() {
		try {
			mutuelleTest = new Mutuelle(1, "Miltis", "Rhône", "Lyon", "69003", "Cr Albert Thomas",
					"0727852788", "mutuelle@miltis.fr", "40");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("Le remboursement n'est pas bon. Insérez un chiffre valide!"));
		}
	}

	@Test
	public void testToString() throws MissMatchException {
		mutuelleTest = new Mutuelle(1, "Miltis", "Rhône", "Lyon", "69003", "Cr Albert Thomas",
				"0727852788", "mutuelle@miltis.fr", "40.0");
		assertEquals("Miltis", mutuelleTest.toString());
	}
}

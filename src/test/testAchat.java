package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import exception.MissMatchException;
import model.Achat;
import model.Medicament;

public class testAchat {
	Achat achatTest;
	Medicament medic;

	@Test
	public void testConstructorAchat() throws MissMatchException {
		medic = new Medicament(1, "Gentamicine", "Antibiotique", "2024-11-25", "10.00", "200");
		achatTest = new Achat(2, "Bayat", "Ehsan", "125425478595632", "Dr. Philipe", "25/01/2022",
				"Marquis", medic, "CIF", 5, 10.00, 50.0, 25.0);
		assertEquals("Bayat", achatTest.getNomClient());
		assertEquals("Ehsan", achatTest.getPrenomClient());
		assertEquals("125425478595632", achatTest.getSecu());
		assertEquals("Dr. Philipe", achatTest.getMedecin());
		assertEquals("25/01/2022", achatTest.getDateAchat());
		assertEquals("Marquis", achatTest.getSpecialiste());
		assertEquals("Paracétamol", achatTest.getMedicament());
		assertEquals("CIF", achatTest.getMutuelle());
		assertEquals(5, achatTest.getQuantiteMedicament());
		assertEquals(10.00, achatTest.getPrix());
		assertEquals(50.0, achatTest.getRemboursement());
		assertEquals(25.0, achatTest.getPrixTotal());
		assertEquals(2, achatTest.getIdAchat());
	}

	@Test
	public void testConstructorAchatSansID() throws MissMatchException {
		medic = new Medicament(1, "Gentamicine", "Antibiotique", "2024-11-25", "10.00", "200");
		achatTest = new Achat("Bayat", "Ehsan", "125425478595632", "Dr. Philipe", "25/01/2022",
				"Marquis", medic, "CIF", 5, 10.00, 50.0, 25.0);
		assertEquals("Bayat", achatTest.getNomClient());
		assertEquals("Ehsan", achatTest.getPrenomClient());
		assertEquals("125425478595632", achatTest.getSecu());
		assertEquals("Dr. Philipe", achatTest.getMedecin());
		assertEquals("25/01/2022", achatTest.getDateAchat());
		assertEquals("Marquis", achatTest.getSpecialiste());
		assertEquals("Paracétamol", achatTest.getMedicament());
		assertEquals("CIF", achatTest.getMutuelle());
		assertEquals(5, achatTest.getQuantiteMedicament());
		assertEquals(10.00, achatTest.getPrix());
		assertEquals(50.0, achatTest.getRemboursement());
		assertEquals(25.0, achatTest.getPrixTotal());
	}

	@Test
	public void testToString() throws MissMatchException {
		medic = new Medicament(1, "Gentamicine", "Antibiotique", "2024-11-25", "10.00", "200");
		achatTest = new Achat("Bayat", "Ehsan", "125425478595632", "Dr. Philipe", "25/01/2022",
				"Marquis", medic, "CIF", 5, 10.00, 50.0, 25.0);

		assertEquals("Nom: " + "Bayat" + "\nPrénom: " + "Ehsan" + "\nN° Sécu: " + "125425478595632"
				+ "\nMédecin Traitant: " + "Dr. Philipe" + "\nDate d'achat: " + "25/01/2022"
				+ "\nSpécialiste: " + "Marquis" + "\nMédicament: " + "Paracétamol" + "\nMutuelle: "
				+ "CIF" + "\nQuantité Médicament: " + 5 + "\nLe prix total: " + 25.0,
				achatTest.toString());
	}
}

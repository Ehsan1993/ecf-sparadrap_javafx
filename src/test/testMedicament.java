package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import exception.MissMatchException;
import model.Medicament;

public class testMedicament {

	Medicament medicamentTest;

	@Test
	public void testMissMatch() throws MissMatchException {

		medicamentTest = new Medicament(1, "Gentamicine", "Antibiotique", "2023-03-24", "10.0",
				"250");
		assertEquals(1, medicamentTest.getIdMedicament());
		assertEquals(250, medicamentTest.getQuantite());
		assertEquals("Gentamicine", medicamentTest.toString());
		assertEquals("Gentamicine", medicamentTest.getNom());
		assertEquals("Antibiotique", medicamentTest.getCategorie());
		assertEquals(10.0, medicamentTest.getPrix());
		assertEquals("2023-03-24", medicamentTest.getDateService().toString());
	}

	@Test
	public void testNom() throws MissMatchException {

		try {
			medicamentTest = new Medicament(1, "Gent.amicine", "Antibiotique", "2023-03-24", "10.0",
					"250");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("Le nom médicament n'est pas bon. Insérez un nom valide!"));
		}
	}

	@Test
	public void testCategorie() throws MissMatchException {

		try {
			medicamentTest = new Medicament(1, "Gentamicine", "Antib.iotique", "2023-03-24", "10.0",
					"250");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("la catagorie de médicament pas bonne. Insérez un nom valide!"));
		}
	}

	@Test
	public void testPrix() throws MissMatchException {

		try {
			medicamentTest = new Medicament(1, "Gentamicine", "Antibiotique", "2023-03-24", "10",
					"250");
		} catch (MissMatchException e) {
			assert (e.getMessage().contains("Le prix n'est pas bon. Insérez un prix valide!"));
		}
	}

	@Test
	public void testQuantite() throws MissMatchException {

		try {
			medicamentTest = new Medicament(1, "Gentamicine", "Antibiotique", "2023-03-24", "10.0",
					"250.3");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("La Quanité n'est pas bonne. Insérez une quantité valide!"));
		}
	}

}

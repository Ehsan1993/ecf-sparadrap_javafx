package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import exception.MissMatchException;
import model.Medecin;

class testMedecin {

	Medecin medecinTest;

	@Test
	public void testGetAgreement() throws MissMatchException {

		medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
				"0383940327", "lilas@gmail.fr", "43435445345");

		assertEquals("43435445345", medecinTest.getNumAgreement());
	}

	@Test
	public void testIdMedecin() throws MissMatchException {

		medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
				"0383940327", "lilas@gmail.fr", "43435445345");

		assertEquals(1, medecinTest.getId_Medecin());
	}

	@Test
	public void testNullNom() throws MissMatchException {

		try {
			medecinTest = new Medecin(1, null, "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un nom!"));
		}
	}

	@Test
	public void testFormatNom() throws MissMatchException {
		try {
			medecinTest = new Medecin(1, "Mez iane", "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "43435445345");
		} catch (MissMatchException e) {
			assert (e.getMessage().contains("Le nom n'est pas bon. Insérez un nom valide!"));
		}
	}

	@Test
	public void testNullPrenom() throws MissMatchException {

		try {
			medecinTest = new Medecin(1, "Meziane", null, "Nancy", "54000", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un prénom!"));
		}
	}

	@Test
	public void testEmptyPrenom() throws MissMatchException {
		try {
			medecinTest = new Medecin(1, "Meziane", "", "Nancy", "54000", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un prénom!"));
		}
	}

	@Test
	public void testFormatPrenom() throws MissMatchException {
		try {
			medecinTest = new Medecin(1, "Meziane", "Lil as", "Nancy", "54000", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "43435445345");
		} catch (MissMatchException e) {
			assert (e.getMessage().contains("Le prénom n'est pas bon. Insérez un prénom valide!"));
		}
	}

	@Test
	public void testNullAdresse() throws MissMatchException {

		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "54000", null, "0383940327",
					"lilas@gmail.fr", "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un adresse!"));
		}
	}

	@Test
	public void testNullCodePostal() throws MissMatchException {

		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", null, "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir le code postal!"));
		}
	}

	@Test
	public void testEmptyCodePostal() throws MissMatchException {
		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir le code postal!"));
		}
	}

	@Test
	public void testFormatCodePostal() throws MissMatchException {
		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "540000", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "43435445345");
		} catch (MissMatchException e) {
			assert (e.getMessage().contains(
					"Le code postal n'est pas bon. Un code postal a forcement 5 chiffres!"));
		}
	}

	@Test
	public void testNullVille() throws MissMatchException {

		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", null, "54000", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir le nom de la ville!"));
		}
	}

	@Test
	public void testEmptyVille() throws MissMatchException {
		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "", "54000", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir le nom de la ville!"));
		}
	}

	@Test
	public void testFormatVille() throws MissMatchException {
		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nan cy", "54000", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "43435445345");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("Le nom de ville n'est pas bon. Insérez un nom valide!"));
		}
	}

	@Test
	public void testNullPhone() throws MissMatchException {

		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
					null, "lilas@gmail.fr", "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un numéro de téléphone!"));
		}
	}

	@Test
	public void testEmptyPhone() throws MissMatchException {
		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
					"", "lilas@gmail.fr", "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir un numéro de téléphone!"));
		}
	}

	@Test
	public void testFormatPhone() throws MissMatchException {
		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
					"03839402327", "lilas@gmail.fr", "43435445345");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("Le numéro de téléphone n'est pas bon. Insérez un numéro valide!"));
		}
	}

	@Test
	public void testNullMail() throws MissMatchException {

		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
					"0383940327", null, "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir l'adresse mail!"));
		}
	}

	@Test
	public void testEmptyMail() throws MissMatchException {
		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
					"0383940327", "", "43435445345");
		} catch (NumberFormatException e) {
			assert (e.getMessage().contains("Veuillez saisir l'adresse mail!"));
		}
	}

	@Test
	public void testFormatMail() throws MissMatchException {
		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail", "43435445345");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("L'adresse mail n'est pas bon. Insérez une adresse mail valide!"));
		}
	}

	@Test
	public void testAgreement() {

		try {
			medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
					"0383940327", "lilas@gmail.fr", "434355445345");
		} catch (MissMatchException e) {
			assert (e.getMessage()
					.contains("Un numéro d'agrément contient forcement 11 chiffres!"));
		}
	}

	@Test
	public void testToString() throws MissMatchException {
		medecinTest = new Medecin(1, "Meziane", "Lilas", "Nancy", "54000", "5, TER Pl. Carnot",
				"0383940327", "lilas@gmail.fr", "43435445345");
		assertEquals("Dr. " + "MEZIANE", medecinTest.toString());
	}
}

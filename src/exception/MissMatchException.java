package exception;

/**
 * 
 * @author User-06
 *
 */
public class MissMatchException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7921635204325068575L;

	/**
	 * 
	 * @param message
	 */
	public MissMatchException(String message) {
		super(message);
	}

	/**
	 * 
	 * @param cause
	 */
	public MissMatchException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public MissMatchException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public MissMatchException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
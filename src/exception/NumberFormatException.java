package exception;

/**
 * 
 * @author User-06
 *
 */
public class NumberFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2686408653436225694L;

	/**
	 * 
	 */
	public NumberFormatException() {

	}

	/**
	 * 
	 * @param message
	 */
	public NumberFormatException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param cause
	 */
	public NumberFormatException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public NumberFormatException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public NumberFormatException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}

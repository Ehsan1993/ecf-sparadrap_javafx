package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import connection.DatabaseConnection;
import model.Medecin;

/**
 * @author User-06
 *
 */
public class MedecinDAO {

	/**
	 * @return
	 */
	public static List<Medecin> recupererMedecin() {
		List<Medecin> liste_Medecin = new ArrayList<>();
		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "SELECT* FROM Medecin";
			PreparedStatement stat = connection.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();
			while (rs.next()) {

				Integer idMedecin = rs.getInt("id_Medecin");
				String nomMedecin = rs.getString("NomMedecin");
				String prenomMedecin = rs.getString("Prenom");
				String villeMedecin = rs.getString("Ville");
				String codePostalMedecin = rs.getString("CodePostal");
				String adresseMedecin = rs.getString("Adresse");
				String phoneMedecin = rs.getString("Phone");
				String mailMedecin = rs.getString("Email");
				String numAgreementMedecin = rs.getString("numAgreement");

				liste_Medecin.add(new Medecin(idMedecin, nomMedecin, prenomMedecin, villeMedecin,
						codePostalMedecin, adresseMedecin, phoneMedecin, mailMedecin,
						numAgreementMedecin));
			}
			connection.close();
			return liste_Medecin;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import connection.DatabaseConnection;
import model.Medicament;

/**
 * @author User-06
 *
 */
public class MedicamentDAO {

	/**
	 * @return
	 */
	public static List<Medicament> recupererMedicament() {
		List<Medicament> liste_Medicament = new ArrayList<>();

		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "SELECT* FROM medicament";
			PreparedStatement stat = connection.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();
			while (rs.next()) {

				Integer idMedicament = rs.getInt("id_Medicament");
				String nomMedicament = rs.getString("NomMedicament");
				String categorie = rs.getString("CategorieMedicament");
				String dateService = rs.getString("DateService");
				Double prixMedicament = rs.getDouble("PrixMedicament");
				String quantiteMedicament = String.valueOf(rs.getInt("QuantiteMedicament"));

				String prix = String.valueOf(prixMedicament);

				liste_Medicament.add(new Medicament(idMedicament, nomMedicament, categorie,
						dateService, prix, quantiteMedicament));
			}
			connection.close();
			return liste_Medicament;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List<Medicament> recupererMedicamentParID(Integer id) {
		List<Medicament> liste_Medicament = new ArrayList<>();

		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "SELECT* FROM medicament WHERE id_Medicament LIKE " + "\'" + id + "\'";
			PreparedStatement stat = connection.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();
			while (rs.next()) {

				Integer idMedicament = rs.getInt("id_Medicament");
				String nomMedicament = rs.getString("NomMedicament");
				String categorie = rs.getString("CategorieMedicament");
				String dateService = rs.getString("DateService");
				Double prixMedicament = rs.getDouble("PrixMedicament");
				String quantiteMedicament = String.valueOf(rs.getInt("QuantiteMedicament"));

				String prix = String.valueOf(prixMedicament);

				liste_Medicament.add(new Medicament(idMedicament, nomMedicament, categorie,
						dateService, prix, quantiteMedicament));
			}
			connection.close();
			return liste_Medicament;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}

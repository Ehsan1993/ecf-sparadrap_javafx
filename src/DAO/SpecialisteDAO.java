package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import connection.DatabaseConnection;
import model.Specialiste;

/**
 * @author User-06
 *
 */
public class SpecialisteDAO {

	/**
	 * @return
	 */
	public static List<Specialiste> recupererSpecialiste() {
		List<Specialiste> liste_Specialiste = new ArrayList<>();

		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "SELECT* FROM specialiste";
			PreparedStatement stat = connection.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();
			while (rs.next()) {

				Integer idSpecialiste = rs.getInt("id_Specialiste");
				String nomSpecialiste = rs.getString("NomSpecialiste");
				String prenomSpecialiste = rs.getString("Prenom");
				String villeSpecialiste = rs.getString("Ville");
				String codePostalSpecialiste = rs.getString("CodePostal");
				String adresseSpecialiste = rs.getString("Adresse");
				String phoneSpecialiste = rs.getString("Phone");
				String mailSpecialiste = rs.getString("Email");
				String specialite = rs.getString("Specialite");

				liste_Specialiste.add(new Specialiste(idSpecialiste, nomSpecialiste,
						prenomSpecialiste, villeSpecialiste, codePostalSpecialiste,
						adresseSpecialiste, phoneSpecialiste, mailSpecialiste, specialite));
			}
			connection.close();
			return liste_Specialiste;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

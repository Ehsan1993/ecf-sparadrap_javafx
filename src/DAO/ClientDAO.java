package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.DatabaseConnection;
import exception.MissMatchException;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import model.Client;

/**
 * @author User-06
 *
 */
public class ClientDAO {

	/**
	 * @param client
	 * @throws MissMatchException
	 */
	public static void enregistrerClient(Client client) throws MissMatchException {
		try {
			Connection connection = DatabaseConnection.getConnection();

			String sql = "INSERT INTO clients (NomClient, PrenomClient, Date_Naissance, Ville, Code_Postal, Adresse, Phone, Mail, Securite_Sociale, id_Mutuelle, id_Medecin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			PreparedStatement stat = connection.prepareStatement(sql);

			stat.setString(1, client.getNom());
			stat.setString(2, client.getPrenom());
			stat.setString(3, client.getDateDeNaissance());
			stat.setString(4, client.getVille());
			stat.setString(5, client.getCodePostal());
			stat.setString(6, client.getAdresse());
			stat.setString(7, client.getPhone());
			stat.setString(8, client.getMail());
			stat.setString(9, client.getNumSecuriteSociale());
			stat.setInt(10, Integer.parseInt(client.getMutuelle()));
			stat.setInt(11, Integer.parseInt(client.getMedecinTraitant()));

			stat.executeUpdate();

			System.out.println(client.getNom() + " " + client.getPrenom()
					+ " a été enregistré dans la Base de Donnée.");
			stat.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Enregistrement du client a été échouée.");
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur");
			alert.setHeaderText("Enregistrement échoué");
			alert.setContentText("Vérifiez si le client existe dans la base de donné!!");
			alert.showAndWait();
		} catch (NumberFormatException e1) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur");
			alert.setHeaderText("Erreur de Format");
			alert.setContentText(e1.getMessage());
			alert.showAndWait();
		} catch (NullPointerException NPE) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur");
			alert.setHeaderText("Manque d'information!!!");
			alert.setContentText("Veuillez remplir les champs!!!");
			alert.showAndWait();
		}
	}

	/**
	 * @return
	 */
	public static List<Client> recupererClient() {

		List<Client> liste_Client = new ArrayList<Client>();
		try {
			Connection connection = DatabaseConnection.getConnection();

			String sql = "SELECT * FROM clients c JOIN mutuelle m ON c.id_Mutuelle = m.id_Mutuelle JOIN medecin mt ON c.id_Medecin = mt.id_Medecin";
			PreparedStatement stat = connection.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();

			while (rs.next()) {

				Integer idClient = rs.getInt("id_Client");
				String nomSQL = rs.getString("NomClient");
				String prenomSQL = rs.getString("PrenomClient");
				String naissanceSQL = rs.getString("Date_Naissance");
				String villeSQL = rs.getString("Ville");
				String postalSQL = rs.getString("Code_Postal");
				String adresseSQL = rs.getString("Adresse");
				String phoneSQL = rs.getString("Phone");
				String mailSQL = rs.getString("Mail");
				String secuSQL = rs.getString("Securite_Sociale");
				String mutuelleSQL = rs.getString("NomMutuelle");
				String medecinSQL = rs.getString("NomMedecin");

				liste_Client.add(
						new Client(idClient, nomSQL, prenomSQL, naissanceSQL, villeSQL, postalSQL,
								adresseSQL, phoneSQL, mailSQL, secuSQL, mutuelleSQL, medecinSQL));
			}
			connection.close();
			return liste_Client;

		} catch (SQLException | MissMatchException e) {
			e.printStackTrace();
			return null;
		}
	}
}

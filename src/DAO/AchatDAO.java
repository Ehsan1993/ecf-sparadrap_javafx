package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.DatabaseConnection;
import model.Achat;
import model.Medicament;

/**
 * 
 * @author User-06
 *
 */
public class AchatDAO {
	/**
	 * 
	 * @param idClient
	 * @param dateAchat
	 * @param mutuelleId
	 */
	// Remplir la table Achat à la BDD
	public static void EnregistrerAchat(Integer idClient, String dateAchat, Integer mutuelleId) {

		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "INSERT INTO achat (dateAchat, id_Client, id_Mutuelle) VALUES (?, ?, ?)";
			PreparedStatement stat = connection.prepareStatement(sql);

			stat.setString(1, dateAchat);
			stat.setInt(2, idClient);

			if (mutuelleId != null) {
				stat.setInt(3, mutuelleId);
			} else {
				stat.setString(3, null);
			}
			stat.executeUpdate();
			stat.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Récupérer id_Achat de la BDD
	 * 
	 * @param idClient
	 * @return
	 */
	public static List<Integer> recupererIdAchat(Integer idClient) {
		List<Integer> liste_IdAchat = new ArrayList<>();
		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "SELECT max(id_Achat) FROM achat WHERE id_Client LIKE" + "\'" + idClient
					+ "\'";
			PreparedStatement stat = connection.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();

			while (rs.next()) {

				Integer idAchat = rs.getInt(1);
				liste_IdAchat.add(idAchat);
			}
			connection.close();
			return liste_IdAchat;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Remplir la table Ordonnance en base de donnée
	 * 
	 * @param dateAchat
	 * @param idAchat
	 * @param idSpecialiste
	 */
	public static void EnregistrerOrdonnance(String dateAchat, Integer idAchat,
			Integer idSpecialiste) {

		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "INSERT INTO ordonnance (dateOrdonnance, id_Achat, id_Specialiste) VALUES (?, ?, ?)";

			PreparedStatement stat = connection.prepareStatement(sql);

			stat.setString(1, dateAchat);
			stat.setInt(2, idAchat);

			if (idSpecialiste != null) {
				stat.setInt(3, idSpecialiste);
			} else {
				stat.setString(3, null);
			}
			stat.executeUpdate();
			stat.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Remplir la liste médicament en base de donnée
	/**
	 * @param idAchat
	 * @param idMedicament
	 * @param quantite
	 */
	public static void enregistrerMedicamentAchete(Integer idAchat, Integer idMedicament,
			Integer quantite) {

		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "INSERT INTO liste_medicament (id_Achat, id_Medicament, quantite_Medicament) VALUES (?, ?, ?)";

			PreparedStatement stat = connection.prepareStatement(sql);

			stat.setInt(1, idAchat);
			stat.setInt(2, idMedicament);
			stat.setInt(3, quantite);

			stat.executeUpdate();
			stat.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return
	 */
	public static List<Achat> recupererlisteAchat() {
		List<Achat> liste_Achat = new ArrayList<>();
		try {
			Connection connection = DatabaseConnection.getConnection();
			StringBuilder sql2 = new StringBuilder();
			sql2.append(
					"SELECT * FROM achat a LEFT JOIN  ordonnance ord ON a.id_Achat = ord.id_Achat ");
			sql2.append("LEFT JOIN clients c ON a.id_Client = c.id_Client ");
			sql2.append("LEFT JOIN liste_medicament lm ON a.id_Achat = lm.id_Achat ");
			sql2.append("JOIN medicament m ON m.id_Medicament = lm.id_Medicament ");
			sql2.append("LEFT JOIN mutuelle mu ON mu.id_Mutuelle = a.id_Mutuelle ");
			sql2.append("JOIN medecin me ON me.id_Medecin = c.id_Medecin ");
			sql2.append("LEFT JOIN specialiste sp ON ord.id_Specialiste = sp.id_Specialiste ");

			PreparedStatement stat = connection.prepareStatement(sql2.toString());
			ResultSet rs = stat.executeQuery();

			while (rs.next()) {

				String nomClient = rs.getString("NomClient");
				String prenomClient = rs.getString("PrenomClient");
				String secuClient = rs.getString("Securite_Sociale");
				String medecinClient = rs.getString("NomMedecin");
				String specialisteClient = rs.getString("NomSpecialiste");
				String mutuelleClient = rs.getString("NomMutuelle");
				String dateAchatClient = rs.getString("dateAchat");
				Integer quMedicClient = rs.getInt("quantite_Medicament");
				Integer idMedicament = rs.getInt("id_Medicament");
				Double remboursement = rs.getDouble("Remboursement");
				Double prix = rs.getDouble("PrixMedicament");

				Double total = (prix - ((remboursement / 100) * prix)) * quMedicClient;
				Double prixTot = Math.round(total * 100.0) / 100.0;

				Medicament medicament = MedicamentDAO.recupererMedicamentParID(idMedicament).get(0);
				liste_Achat.add(new Achat(nomClient, prenomClient, secuClient, medecinClient,
						dateAchatClient, specialisteClient, medicament, mutuelleClient,
						quMedicClient, 0.0, 0.0, prixTot));
			}
			connection.close();
			return liste_Achat;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @return
	 */
	public static List<Achat> recupererlisteAchat_Jour() {
		List<Achat> liste_Achat_Jour = new ArrayList<>();
		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "SELECT * FROM achat a"
					+ " LEFT JOIN  ordonnance ord ON a.id_Achat = ord.id_Achat"
					+ " LEFT JOIN clients c ON a.id_Client = c.id_Client"
					+ " LEFT JOIN liste_medicament lm ON a.id_Achat = lm.id_Achat"
					+ " JOIN medicament m ON m.id_Medicament = lm.id_Medicament"
					+ " LEFT JOIN mutuelle mu ON mu.id_Mutuelle = a.id_Mutuelle"
					+ " JOIN medecin me ON me.id_Medecin = c.id_Medecin"
					+ " LEFT JOIN specialiste sp ON ord.id_Specialiste = sp.id_Specialiste WHERE dateAchat = CURDATE() ";

			PreparedStatement stat = connection.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();

			while (rs.next()) {

				String nomClient = rs.getString("NomClient");
				String prenomClient = rs.getString("PrenomClient");
				String secuClient = rs.getString("Securite_Sociale");
				String medecinClient = rs.getString("NomMedecin");
				String specialisteClient = rs.getString("NomSpecialiste");
				String mutuelleClient = rs.getString("NomMutuelle");
				String dateAchatClient = rs.getString("dateAchat");
				Integer quMedicClient = rs.getInt("quantite_Medicament");
				Integer idMedicament = rs.getInt("id_Medicament");
				Double remboursement = rs.getDouble("Remboursement");
				Double prix = rs.getDouble("PrixMedicament");

				Double total = (prix - ((remboursement / 100) * prix)) * quMedicClient;
				Double prixTot = Math.round(total * 100.0) / 100.0;
				Medicament medicament = MedicamentDAO.recupererMedicamentParID(idMedicament).get(0);

				liste_Achat_Jour.add(new Achat(nomClient, prenomClient, secuClient, medecinClient,
						dateAchatClient, specialisteClient, medicament, mutuelleClient,
						quMedicClient, 0.0, 0.0, prixTot));
			}
			connection.close();
			return liste_Achat_Jour;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import connection.DatabaseConnection;
import model.Ordonnance;

/**
 * @author User-06
 *
 */
public class OrdonnanceDAO {

	/**
	 * @return
	 */
	public static List<Ordonnance> recupererOrdonnance() {
		List<Ordonnance> liste_Ordonnance = new ArrayList<>();
		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "SELECT NomClient, PrenomClient, NomSpecialiste, dateOrdonnance, o.id_Achat, group_concat(NomMedicament) as listeMedicaments"
					+ " FROM ordonnance o JOIN achat a ON a.id_Achat = o.id_Achat"
					+ " JOIN clients c ON c.id_Client = a.id_Client"
					+ " JOIN liste_medicament lm ON lm.id_Achat = a.id_Achat"
					+ " JOIN medicament m ON m.id_Medicament = lm.id_Medicament"
					+ " LEFT JOIN specialiste s ON o.id_Specialiste = s.id_Specialiste GROUP BY lm.id_Achat";

			PreparedStatement stat = connection.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();

			while (rs.next()) {

				String nomPatient = rs.getString("NomClient");
				String prenomPatient = rs.getString("PrenomClient");
				String patient = nomPatient + " " + prenomPatient;

				String dateOrdonnance = rs.getString("dateOrdonnance");
				String specialiste = rs.getString("NomSpecialiste");
				Integer idAchat = rs.getInt("id_Achat");
				String listeMedicament = rs.getString("listeMedicaments");

				liste_Ordonnance.add(new Ordonnance(patient, dateOrdonnance, idAchat, specialiste,
						listeMedicament));
			}
			connection.close();
			return liste_Ordonnance;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @param idMedecin
	 * @return
	 */
	public static List<Ordonnance> recupererOrdoParMedecin(Integer idMedecin) {
		List<Ordonnance> liste_Ordonnance = new ArrayList<>();
		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "SELECT NomClient, PrenomClient, NomSpecialiste, dateOrdonnance, o.id_Achat, group_concat(NomMedicament) as listeMedicaments"
					+ " FROM ordonnance o JOIN achat a ON a.id_Achat = o.id_Achat"
					+ " JOIN clients c ON c.id_Client = a.id_Client"
					+ " JOIN liste_medicament lm ON lm.id_Achat = a.id_Achat"
					+ " JOIN medicament m ON m.id_Medicament = lm.id_Medicament"
					+ " LEFT JOIN specialiste s ON o.id_Specialiste = s.id_Specialiste WHERE id_Medecin = "
					+ "\'" + idMedecin + "'GROUP BY lm.id_Achat";

			PreparedStatement stat = connection.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();

			while (rs.next()) {

				String nomPatient = rs.getString("NomClient");
				String prenomPatient = rs.getString("PrenomClient");
				String patient = nomPatient + " " + prenomPatient;

				String dateOrdonnance = rs.getString("dateOrdonnance");
				String specialiste = rs.getString("NomSpecialiste");
				Integer idAchat = rs.getInt("id_Achat");
				String listeMedicament = rs.getString("listeMedicaments");

				liste_Ordonnance.add(new Ordonnance(patient, dateOrdonnance, idAchat, specialiste,
						listeMedicament));
			}
			connection.close();
			return liste_Ordonnance;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

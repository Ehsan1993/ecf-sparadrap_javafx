package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import connection.DatabaseConnection;
import model.Mutuelle;

/**
 * @author User-06
 *
 */
public class MutuelleDAO {

	/**
	 * @return
	 */
	public static List<Mutuelle> recupererMutuelle() {
		List<Mutuelle> liste_Mutuelle = new ArrayList<>();
		try {
			Connection con = DatabaseConnection.getConnection();
			String sql = "SELECT * FROM mutuelle";
			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();

			while (rs.next()) {

				Integer idMutuelle = rs.getInt("id_Mutuelle");
				String nomMutuelle = rs.getString("NomMutuelle");
				String ville = rs.getString("Ville");
				String postal = rs.getString("CodePostal");
				String adresse = rs.getString("Adresse");
				String phone = rs.getString("Phone");
				String mail = rs.getString("Email");
				String departement = rs.getString("Departement");
				Double remb = rs.getDouble("Remboursement");
				String remboursement = String.valueOf(remb);

				liste_Mutuelle.add(new Mutuelle(idMutuelle, nomMutuelle, departement, ville, postal,
						adresse, phone, mail, remboursement));
			}
			con.close();
			return liste_Mutuelle;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @param mutuelleNom
	 * @return
	 */
	public static List<Mutuelle> recupererMutuelle_Achat(String mutuelleNom) {
		List<Mutuelle> liste_Mutuelle_Achat = new ArrayList<>();
		try {
			Connection connection = DatabaseConnection.getConnection();
			String sql = "SELECT * FROM clients c JOIN mutuelle m ON c.id_Mutuelle = m.id_Mutuelle WHERE NomMutuelle LIKE"
					+ " \'" + mutuelleNom + "\'LIMIT 1";

			PreparedStatement stat = connection.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();

			while (rs.next()) {

				Integer idMutuelle = rs.getInt("id_Mutuelle");
				String nomMutuelle = rs.getString("NomMutuelle");
				String ville = rs.getString("Ville");
				String postal = rs.getString("CodePostal");
				String adresse = rs.getString("Adresse");
				String phone = rs.getString("Phone");
				String mail = rs.getString("Email");
				String departement = rs.getString("Departement");
				Double remb = rs.getDouble("Remboursement");
				String remboursement = String.valueOf(remb);

				liste_Mutuelle_Achat.add(new Mutuelle(idMutuelle, nomMutuelle, departement, ville,
						postal, adresse, phone, mail, remboursement));
			}
			connection.close();
			return liste_Mutuelle_Achat;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

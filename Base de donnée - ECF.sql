Drop database IF EXISTS ecf;
create DATABASE ecf;
use ecf;

-------------------- Creating the specialiste table and insert some specialistes into ---------------------------

create table Specialiste(id_Specialiste int auto_increment primary key, 
						NomSpecialiste varchar (50), 
                        Prenom varchar (50), 
                        Ville varchar(50), 
                        CodePostal varchar(5), 
                        Adresse varchar(100), 
                        Phone varchar (15), 
                        Email varchar(50), 
                        Specialite varchar (50));

insert into Specialiste (NomSpecialiste, Prenom, Ville, CodePostal, Adresse, Phone, Email, Specialite) 
value ("Perrin", "Sylvie", "Nancy", "54000", "6 Rue Vauban", "0383901601", "sylvie@gmail.fr", "Dermatoloque"),
("Bassnagel", "Philippe", "Nancy", "54100", "40 Avenue Raymond Pinchard", "0383944346", "philippe@gmail.fr", "Radiologue"),
("saintot", "Manon", "Maxéville", "54320", "132 Rue André Bisiaux", "0383676166", "manon@gmail.fr", "Allergologue"),
("Hay", "Arélien", "Nancy", "54000", "44 Rue des Carmes", "0383191900", "aurelien@gmail.fr", "Ophtalmologue"),
("Amira", "Rouabah", "Nancy", "54000", "95 Rue Ambroise Paré", "0383944008", "amira@gmail.fr", "Pédiatre");

 
 -------------------- Creating the Medecin table and insert some Medecins into ---------------------------
 
create table Medecin(id_Medecin int auto_increment primary key, 
					NomMedecin varchar (50), 
                    Prenom varchar (50), 
                    Ville varchar(50), 
                    CodePostal varchar(5), 
                    Adresse varchar(50), 
                    Phone varchar (15), 
                    Email varchar(50), 
                    numAgreement varchar (11));

insert into Medecin (NomMedecin, Prenom, Ville, CodePostal, Adresse, Phone, Email, numAgreement) 
value ("Meziane", "Lilas", "Nancy", "54000", "5 TER Pl. Carnot", "0383940327", "lilas@gmail.fr", "43435434545"),
("Canton", "Valérie", "Nancy", "54000", "34 Av. Anatole", "0383281757", "valerie@gmail.fr", "48453456435"),
("Steff", "Christine", "Nancy", "54000", " 24 Bd de la Mothe", "0383293647", "christine@gmail.fr", "23987065578");


 -------------------- Creating the Medicament table and insert some Medicaments into ---------------------------
 
create table Medicament(id_Medicament int auto_increment primary key, 
						NomMedicament varchar (50), 
						CategorieMedicament varchar (50), 
                        DateService VARCHAR (10), 
                        prixMedicament Double, 
                        QuantiteMedicament int);

insert into Medicament(NomMedicament, CategorieMedicament, DateService, prixMedicament, QuantiteMedicament)
Value("Lopéramide", "Antidiarrhéiques", "2023-03-27", "15.0", 100),
("Cétrizine", "Antihistaminiques", "2023-02-22", "8.0", 300),
("Hydroxyde-de-magnésium", "Antiacides", "2023-01-03", "12.0", 130),
("Aspirine", "Antiagrégant", "2025-12-05", "5.5", 500),
("Antiacnéïques", "Dermatologie", "2024-03-12", "17.5", 200),
("Alvérine", "Antispasmodiques", "2023-12-13", "14.7", 380),
("Nifédipine", "Antagonistes-du-calcium", "2024-11-25", "13.9", 320),
("Gentamicine", "Antibiotique", "2024-05-26", "10.0", 200);


 -------------------- Creating the Mutuelle table and insert some Meutuelles into ---------------------------
 
create table Mutuelle (id_Mutuelle int auto_increment primary key, 
						NomMutuelle varchar (30), 
                        Ville varchar(30), 
                        CodePostal varchar(5), 
                        Adresse varchar(50), 
                        Phone varchar (15), 
                        Email varchar(30), 
                        Departement varchar (30), 
                        Remboursement Double);

insert into Mutuelle (NomMutuelle, Ville, CodePostal, Adresse, Phone, Email, Departement, Remboursement)
value ("Miltis", "Lyon", "69003", "33 Cr Albert Thomas", "0727852788", "mutuelle@miltis.fr", "Rhône", 40.0),
("Harmonie", "Nancy", "54000", "19 Rue Saint-Jean", "0609808800", "accessibilite@harmonie-mu.fr", "Meurthe&Moselle", 55.0),
                ("MIF", "Paris", "75481", "23 Rue Yves Toudic", "0770157777", "mutuelle@MIF.fr", "Paris", 60.0),
                ("Mutami", "Montpelier", "34000", "16 Cr Gambetta", "0346704535", "mutuelle@mutami.fr", "Hérault", 50.0),
                ("Mut'Est", "Strasbourg", "67000", "11 Bd du Président-Wilson", "0696936323", "mutuelle@mutest.fr", "Bas-Rhin", 60.0);


 -------------------- Creating the Client table and insert some clients into ---------------------------
 
 CREATE TABLE clients (id_Client int AUTO_INCREMENT PRIMARY KEY, 
						NomClient VARCHAR(100), 
                        PrenomClient VARCHAR(100), 
                        Date_Naissance DATE, 
                        Ville VARCHAR(50), 
                        Code_Postal VARCHAR(5), 
                        Adresse VARCHAR (100), 
                        Phone VARCHAR (10), 
                        Mail VARCHAR (50), 
                        Securite_Sociale VARCHAR (15) UNIQUE, 
                        id_Mutuelle INT, 
                        id_Medecin INT, 
                        FOREIGN KEY(id_Mutuelle) REFERENCES mutuelle(id_Mutuelle), 
                        FOREIGN KEY(id_Medecin) REFERENCES medecin (id_Medecin));
 
 insert into clients (NomClient, PrenomClient, Date_Naissance, Ville, Code_Postal, Adresse, Phone, Mail, Securite_Sociale, id_Mutuelle, id_Medecin)
 value ('BAYAT', 'Ehsan', '1993-01-27', 'Maxéville', '54320', '11 rue de la Seille', '0780248467', 'm.ehsanbayat@gmail.com', '125478565478541', 2, 1); 
 
  -------------------- Creating the achat table and insert some achats into ---------------------------
 
 CREATE TABLE achat (id_Achat Int AUTO_INCREMENT PRIMARY KEY, 
					dateAchat date, id_Client int, 
                    id_Mutuelle INT, 
                    FOREIGN KEY(id_Client) REFERENCES clients(id_Client), 
                    FOREIGN KEY (id_Mutuelle) REFERENCES mutuelle (id_Mutuelle));
 
 
   -------------------- Creating the liste_medicament table and insert some listes into ---------------------------
   
CREATE TABLE liste_medicament (id_Achat INT, 
								id_Medicament INT, 
                                quantite_Medicament INT, 
                                FOREIGN KEY (id_Achat) REFERENCES achat(id_Achat), 
                                FOREIGN KEY (id_Medicament) REFERENCES medicament(id_Medicament));
   
   
    -------------------- Creating the liste_medicament table and insert some listes into ---------------------------
 
 CREATE TABLE ordonnance (id_Ordonnance int AUTO_INCREMENT PRIMARY KEY, dateOrdonnance date, id_Achat int, id_Specialiste int, FOREIGN KEY (id_Achat) REFERENCES achat (id_Achat), FOREIGN KEY(id_Specialiste) REFERENCES specialiste(id_Specialiste));

